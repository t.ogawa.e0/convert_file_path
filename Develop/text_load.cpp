//==========================================================================
// テキスト読み取り[text_load.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "text_load.hpp"

//==========================================================================
/**
@brief 指定ファイル内テキスト読み取り
@param Input [in] 読み取りファイル
@return 読み取り結果
*/
std::list<std::string> text_load::get(const std::string & Input) noexcept(false)
{
    std::ifstream ifs(Input); // c++によるファイル読み込み
    std::string str; // 格納string
    std::list<std::string> _input;

    // ファイルが開いたとき
    if (!ifs.fail())
    {
        // 終わりまで回す
        for (;std::getline(ifs, str);) {
            _input.push_back(str);
        }
    }
    else
    {
        std::ofstream ifs(Input); 
    }
    ifs.close();

    return _input;
}
