//==========================================================================
// データの書き込み[data_save.hpp]
// author: tatsuya ogawa
//==========================================================================
#include "data_save.hpp"

//==========================================================================
/**
@brief データの書き込み
@param stdFileName [in] ファイル名
@param InputData [in] 保存するデータ
*/
void data_save::save(const std::string &stdFileName, const std::vector<std::string>&InputData) noexcept(false)
{
    std::ofstream outputfile(stdFileName);
    for (auto &itr : InputData) {
        outputfile << itr << std::endl;
    }
    outputfile.close();
}

//==========================================================================
/**
@brief データの書き込み
@param stdFileName [in] ファイル名
@param InputData [in] 保存するデータ
*/
void data_save::save(const std::string & stdFileName, const std::list<std::string>& InputData) noexcept(false)
{
    std::ofstream outputfile(stdFileName);
    for (auto &itr : InputData) {
        outputfile << itr << std::endl;
    }
    outputfile.close();
}

//==========================================================================
/**
@brief データの書き込み
@param stdFileName [in] ファイル名
@param InputData [in] 保存するデータ
*/
void data_save::save(const std::string & stdFileName, const std::string & InputData) noexcept(false)
{
    std::ofstream outputfile(stdFileName);
    outputfile << InputData;
    outputfile.close();
}
