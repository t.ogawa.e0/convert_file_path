//==========================================================================
// フォルダ探索[folder_path.hpp]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <list>
#include <string>
#include <algorithm>
#include <stdexcept>

namespace folder_path {
    /**
    @brief 実行ファイル直下フォルダ探索
    @return 探索結果
    */
    std::list<std::string> get(void) noexcept(false);

    /**
    @brief 指定フォルダ直下フォルダ探索
    @param folder_name [in] 探索対象のフォルダ
    @return 探索結果
    */
    std::list<std::string> get_in(const std::string& folder_name) noexcept(false);

    /**
    @brief 指定フォルダ直下フォルダの再帰探索
    @param path [in] 探索対象のフォルダ
    @return 探索結果
    */
    std::list<std::string> get_in_recursive(const std::list<std::string> & path) noexcept(false);

    /**
    @brief 実行ファイル直下フォルダ全探索
    @return 探索結果
    */
    std::list<std::string> get_all(void) noexcept(false);
}