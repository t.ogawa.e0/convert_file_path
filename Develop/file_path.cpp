//==========================================================================
// �t�@�C���T��[file_path.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "file_path.hpp"

//==========================================================================
/**
@brief �t�@�C���T��
@param dir_name [in] �T���Ώۃf�B���N�g��
@param extension [in] �g���q�w��
@param set_ [in]
@return �T������
*/
std::list<std::string> file_path::get_in(const std::string& dir_name, const std::string& extension, std::string set_) noexcept(false)
{
    HANDLE hFind = nullptr;
    WIN32_FIND_DATA win32fd;//defined at Windwos.h
    std::list<std::string> file_names;

    ZeroMemory(&win32fd, sizeof(WIN32_FIND_DATA));

    //�g���q�̐ݒ�
    std::string search_name = dir_name + set_ + extension;

    hFind = FindFirstFile(search_name.c_str(), &win32fd);

    if (win32fd.dwFileAttributes != 0)
    {
        if (hFind == INVALID_HANDLE_VALUE)
        {
            throw std::runtime_error("file not found");
        }
        do
        {
            if (win32fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            {
            }
            else
            {
                file_names.push_back(win32fd.cFileName);
            }
        } while (FindNextFile(hFind, &win32fd));
    }

    FindClose(hFind);

    return file_names;
}

//==========================================================================
/**
@brief �t�@�C���T��
@param extension [in] �g���q�w��
@return �T������
*/
std::list<std::string> file_path::get(const std::string & extension) noexcept(false)
{
    return get_in("", extension, "*.");
}