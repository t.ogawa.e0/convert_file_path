//==========================================================================
// テキスト読み取り[text_load.hpp]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <string>
#include <list>
#include <algorithm>
#include <fstream>
#include <stdexcept>

namespace text_load {
    /**
    @brief 指定ファイル内テキスト読み取り
    @param Input [in] 読み取りファイル
    @return 読み取り結果
    */
    std::list<std::string> get(const std::string & Input) noexcept(false);
}
