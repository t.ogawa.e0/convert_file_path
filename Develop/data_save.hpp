//==========================================================================
// データの書き込み[data_save.hpp]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <stdexcept>

namespace data_save {
    /**
    @brief データの書き込み
    @param stdFileName [in] ファイル名
    @param InputData [in] 保存するデータ
    */
    void save(const std::string &stdFileName, const std::vector<std::string>&InputData) noexcept(false);

    /**
    @brief データの書き込み
    @param stdFileName [in] ファイル名
    @param InputData [in] 保存するデータ
    */
    void save(const std::string &stdFileName, const std::list<std::string>&InputData) noexcept(false);

    /**
    @brief データの書き込み
    @param stdFileName [in] ファイル名
    @param InputData [in] 保存するデータ
    */
    void save(const std::string &stdFileName, const std::string&InputData) noexcept(false);
}