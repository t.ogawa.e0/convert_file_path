//==========================================================================
// フォルダ探索[folder_path.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "folder_path.hpp"

//==========================================================================
/**
@brief 実行ファイル直下フォルダ探索
@return 探索結果
*/
std::list<std::string> folder_path::get(void) noexcept(false)
{
    HANDLE hFind = nullptr;
    WIN32_FIND_DATA win32fd; //defined at Windwos.h
    std::list<std::string> folder_names;

    ZeroMemory(&win32fd, sizeof(WIN32_FIND_DATA));

    hFind = FindFirstFile("*", &win32fd);

    if (win32fd.dwFileAttributes != 0)
    {
        if (hFind == INVALID_HANDLE_VALUE)
        {
            throw std::runtime_error("file not found");
        }
        do
        {
            if (win32fd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
            {
                std::string str_ = win32fd.cFileName;
                if (str_ != "." && str_ != "..")
                {
                    folder_names.emplace_back(str_);
                }
            }
        } while (FindNextFile(hFind, &win32fd));
    }

    FindClose(hFind);

    return folder_names;
}

//==========================================================================
/**
@brief 指定フォルダ直下フォルダ探索
@param folder_name [in] 探索対象のフォルダ
@return 探索結果
*/
std::list<std::string> folder_path::get_in(const std::string & folder_name) noexcept(false)
{
    HANDLE hFind = nullptr;
    WIN32_FIND_DATA win32fd;//defined at Windwos.h
    std::list<std::string> folder_names;

    ZeroMemory(&win32fd, sizeof(WIN32_FIND_DATA));

    std::string _folder = folder_name + "/*";

    hFind = FindFirstFile(_folder.c_str(), &win32fd);

    if (win32fd.dwFileAttributes != 0)
    {
        if (hFind == INVALID_HANDLE_VALUE)
        {
            throw std::runtime_error("file not found");
        }
        do 
        {
            if (win32fd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY)
            {
                std::string str_ = win32fd.cFileName;
                if (str_ != "." && str_ != "..")
                {
                    folder_names.emplace_back(folder_name + "/" + str_);
                }
            }
        } while (FindNextFile(hFind, &win32fd));
    }

    FindClose(hFind);

    return folder_names;
}

//==========================================================================
/**
@brief 指定フォルダ直下フォルダの再帰探索
@param path [in] 探索対象のフォルダ
@return 探索結果
*/
std::list<std::string> folder_path::get_in_recursive(const std::list<std::string> & path) noexcept(false)
{
    std::list<std::string> str_folder_new;
    std::list<std::string> str_folder;

    //==========================================================================
    // 探索
    //==========================================================================
    for (auto &itr1 : path) {
        auto path_ = get_in(itr1);
        for (auto &itr2 : path_) {
            str_folder_new.emplace_back(itr2);
        }
    }

    //==========================================================================
    // データの結合
    //==========================================================================
    for (auto &itr : path) {
        str_folder.emplace_back(itr);
    }
    for (auto &itr : str_folder_new) {
        str_folder.emplace_back(itr);
    }

    //==========================================================================
    // 再帰前にデータの最適化
    //==========================================================================
    str_folder.sort(std::greater<std::string>());
    str_folder.unique();

    //==========================================================================
    // 探索するデータが存在する場合データを引き継ぎ再帰する
    //==========================================================================
    if (str_folder_new.size() != 0) {
        str_folder_new = get_in_recursive(str_folder_new);
        for (auto &itr : str_folder_new) {
            str_folder.emplace_back(itr);
        }
    }

    //==========================================================================
    // データを戻す前に最適化
    //==========================================================================
    str_folder.sort(std::greater<std::string>());
    str_folder.unique();

    return str_folder;
}

//==========================================================================
/**
@brief 実行ファイル直下フォルダ全探索
@return 探索結果
*/
std::list<std::string> folder_path::get_all(void) noexcept(false)
{
    return get_in_recursive(get());
}
