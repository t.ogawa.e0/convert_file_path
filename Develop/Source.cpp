//==========================================================================
// convert_file_path
// author: tatsuya ogawa
//==========================================================================

//==========================================================================
// include
//==========================================================================
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#define _GLIBCXX_DEBUG
#include <time.h>
#include <iostream>
#include <chrono>
#include <direct.h>

//==========================================================================
// include hpp
//==========================================================================
#include "data_save.hpp"
#include "file_path.hpp"
#include "folder_path.hpp"
#include "text_load.hpp"

//==========================================================================
// クラス定義
//==========================================================================
class contena_dc
{
public:
    contena_dc() {
        this->m_str_name.clear();
        this->m_str_path.clear();
        this->m_str_extension.clear();
    }
    contena_dc(const std::string &str_name, const std::string &str_path, const std::string &str_extension) {
        this->m_str_name = str_name;
        this->m_str_path = str_path;
        this->m_str_extension = str_extension;
    }
    ~contena_dc() {
        this->m_str_name.clear();
        this->m_str_path.clear();
        this->m_str_extension.clear();
    }
public:
    std::string m_str_name; // ファイル名
    std::string m_str_path; // ファイルパス
    std::string m_str_extension; // 拡張子
};

//==========================================================================
// 型定義
//==========================================================================
using list_contena_dc = std::list<contena_dc>;
using list_string = std::list<std::string>;

//==========================================================================
// 関数定義
//==========================================================================

//==========================================================================
/**
@brief ファイル探索
@param dir_name [in] 探索対象ディレクトリ
@param extension [in] 拡張子指定
@param set_ [in]
*/
list_contena_dc search(
    const std::string& dir_name,
    const list_string& extension,
    const std::string set_
);
//==========================================================================

//==========================================================================
/**
@brief ヘッダー生成
@param create_day [in] 生成日
@param create_time [in] 生成時間
@param data_dc [in] 読み取り結果
@param extension [in] 拡張子指定
*/
void create_header(
    const std::string& create_day,
    const std::string& create_time,
    const list_contena_dc& data_dc,
    const list_string& extension
);
//==========================================================================

//==========================================================================
/**
@brief ログ生成
@param start_ms [in] 生成開始時間
@param end_ms [in] 生成終了時間
@param create_day [in] 生成日
@param create_time [in] 生成時間
@param directory [in] 探索したディレクトリ
@param data_dc [in] 読み取り結果
*/
void create_log(
    float start_ms,
    float end_ms,
    const std::string& create_day,
    const std::string& create_time,
    const list_string& directory,
    const list_contena_dc& data_dc
);
//==========================================================================

//==========================================================================
/**
@brief 時間取得
@return 時間
*/
float get_time_sec(void);
//==========================================================================

//=============================================================================
// main
int main(void)
{
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

    list_contena_dc data_dc; // データ格納用
    time_t timer; 
    tm *date;
    std::string create_day; // 生成日
    std::string create_time; // 生成時間
    char str[256] = { 0 };
    float start_ms = get_time_sec();

    //=============================================================================
    // 処理実行開始時間を記録
    //=============================================================================
    timer = time(nullptr); // 経過時間を取得
    date = localtime(&timer); // 経過時間を時間を表す構造体 date に変換
    strftime(str, sizeof(str), "%Y/%m/%d", date); // 年月日時分秒
    create_day = str;
    strftime(str, sizeof(str), "%H:%M:%S", date); // 年月日時分秒
    create_time = str;

    //=============================================================================
    // メイン処理
    //=============================================================================

    //=============================================================================
    // ディレクトリ探索
    //=============================================================================

    // 拡張子読み取り
    auto extension = text_load::get("拡張子指定.txt");

    // 同じディレクトリ内を探索
    auto directory = folder_path::get_all();

    // ディレクトリ指定なし
    data_dc = search("", extension, "*.");

    // ディレクトリ指定あり
    for (auto &itr1 : directory) {
        // 検出したディレクトリ内の全データを探索する
        auto data_list = search(itr1, extension, "\\*.");
        // 取得した結果を追加する
        for (auto &itr2 : data_list) {
            data_dc.push_back(itr2);
        }
    }

    //=============================================================================
    // 出力処理
    //=============================================================================
    create_header(create_day, create_time, data_dc, extension);

    //=============================================================================
    // ログ出力
    //=============================================================================
    create_log(start_ms, get_time_sec(), create_day, create_time, directory, data_dc);

    return std::system("PAUSE");
}

//=============================================================================
/**
@brief ファイル探索
@param dir_name [in] 探索対象ディレクトリ
@param extension [in] 拡張子指定
@param set_ [in]
*/
list_contena_dc search(
    const std::string & dir_name,
    const list_string& extension,
    const std::string set_
)
{
    list_contena_dc data_dc; // データ格納用

    // 探索データ記録
    for (auto &itr1 : extension) {

        // 全探索対象ディレクトリ内を検索
        auto data = file_path::get_in(dir_name, itr1, set_);

        // データの格納
        for (auto &itr2 : data) {
            std::string _file_name; // ファイル名
            std::string _path_name; // 拡張子
            bool bchange = false;

            // 拡張子排除
            for (auto &itr3 : itr2) {
                std::string str_;
                str_ = itr3;

                if (str_ == ".") {
                    bchange = true;
                }
                if (bchange == false && str_ != ".") {
                    _file_name += str_;
                }
                else if (bchange == true && str_ != ".") {
                    _path_name += str_;
                }
            }

            if (dir_name == "") {
                data_dc.emplace_back(_file_name, dir_name + itr2, _path_name);
            }
            else if (dir_name != "") {
                data_dc.emplace_back(_file_name, dir_name + "/" + itr2, _path_name);
            }
        }
    }
    return data_dc;
}

//==========================================================================
/**
@brief ヘッダー生成
@param create_day [in] 生成日
@param create_time [in] 生成時間
@param data_dc [in] 読み取り結果
@param extension [in] 拡張子指定
*/void create_header(
    const std::string & create_day,
    const std::string & create_time,
    const list_contena_dc & data_dc,
    const list_string& extension
)
{
    list_string header_data; // ヘッダーの内容
    const std::string _define = "#define ";
    const std::string _ifndef = "#ifndef ";
    const std::string _endif = "#endif ";
    std::string _extension;
    int n_count = 0;

    header_data.push_back("//=============================================================================");
    header_data.push_back("// resource file path");
    header_data.push_back("// " + create_day);
    header_data.push_back("// " + create_time);
    header_data.push_back("//=============================================================================");
    header_data.push_back("#pragma once");

    for (auto &itr1 : extension) {
        for (auto &itr2 : data_dc) {
            if (itr1 == itr2.m_str_extension) {

                std::string header_text; // テキスト生成用

                if (_extension != itr1) {
                    _extension = itr1;
                    header_data.push_back("");
                    header_data.push_back("//=============================================================================");
                    header_data.push_back("// filename extension [." + _extension + "]");
                    header_data.push_back("//=============================================================================");
                    header_data.push_back("");
                }

                header_text = "RESOURCE_";
                header_text += itr2.m_str_name;
                header_text += "_";
                header_text += itr2.m_str_extension;
                header_data.push_back(_ifndef + header_text);
                header_data.push_back(_define + header_text + " " + '"' + itr2.m_str_path + '"');
                header_data.push_back(_endif + "// !" + header_text);
                header_text.clear();
            }
        }
    }

    //=============================================================================
    // 出力
    //=============================================================================
    list_string str_link;

    // txtデータの生成
    str_link.push_back(std::to_string(data_dc.size()));
    for (auto &itr : data_dc) {
        // 格納
        str_link.push_back(itr.m_str_path);
    }

    // 保存
    data_save::save("resource_list.txt", str_link);
    data_save::save("resource_list.h", header_data);
}

//==========================================================================
/**
@brief ログ生成
@param start_ms [in] 生成開始時間
@param end_ms [in] 生成終了時間
@param create_day [in] 生成日
@param create_time [in] 生成時間
@param directory [in] 探索したディレクトリ
@param data_dc [in] 読み取り結果
*/
void create_log(
    float start_ms,
    float end_ms,
    const std::string & create_day,
    const std::string & create_time,
    const list_string & directory,
    const list_contena_dc & data_dc
)
{
    std::string Generation_time = "GenerationTime : " + std::to_string(end_ms - start_ms) + " ms";
    list_string log_data;
    log_data.push_back("");
    log_data.push_back("=========== convert_file_path ===========");
    log_data.push_back("*** " + create_day);
    log_data.push_back("*** " + create_time);
    log_data.push_back("*** " + Generation_time);
    log_data.push_back("*** Number of detected data : " + std::to_string((int)data_dc.size()));
    log_data.push_back("");
    log_data.push_back("=========== We are generating logs ===========");
    log_data.push_back("");
    log_data.push_back("■■■■■■■■ Folder import list ■■■■■■■■");
    log_data.push_back("");
    for (auto &itr : directory) {
        log_data.push_back("[" + itr + "]" + "を検出しました");
    }
    log_data.push_back("");
    log_data.push_back("■■■■■■■■ File import list ■■■■■■■■");
    log_data.push_back("");
    for (auto &itr : data_dc) {
        log_data.push_back("[" + itr.m_str_path + "]" + "を検出しました");
    }
    log_data.push_back("");
    log_data.push_back("=========== Log generation has ended ===========");
    log_data.push_back("");
    log_data.push_back("**** Crash! ****");
    log_data.push_back("");

    // ログの保存
    data_save::save("convert_file_path.log", log_data);

    // 出力
    for (auto &itr : log_data) {
        std::cout << itr << std::endl;
    }
}

//==========================================================================
/**
@brief 時間取得
@return 時間
*/
float get_time_sec(void)
{
    return static_cast<float>(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()) / (float)1000000000;
}
