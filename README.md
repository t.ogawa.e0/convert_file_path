convert_file_path
====

*指定した拡張子のデータを検索し、ヘッダーを生成します。<br>*
*検索開始地点はプログラムが置いてあるフォルダディレクトリからです。<br>*
*フォルダが存在する場合、存在するフォルダ全て検索し、その内部に指定した拡張子が存在するかどうかの検索を行います。<br>*
*ファイルパスは相対パスで生成されます。*

*  [Execution Data](Execution%20Data) - 実行データです。
*  [Develop](Develop) - 開発データです。

このプログラムが生成するデータ
====
*  convert_file_path.log　ログの出力です。
*  resource_list.h　リソースへのファイルパスをマクロ定義したヘッダです。
*  resource_list.txt　ファイルパスを .txt に書き込んだファイルです。
*  拡張子指定.txt　検索したい拡張子を記述するファイルです。ファイルがない場合生成されます。